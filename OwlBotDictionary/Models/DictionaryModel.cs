﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OwlBotDictionary.Models
{//This is response model of owlbot api
	
        public class Definition
        {
            public string type { get; set; }
            public string definition { get; set; }
            public string example { get; set; }
            public string image_url { get; set; }
            public string emoji { get; set; }
        }

        public class DictionaryModel
        {
            public List<Definition> definitions { get; set; }
            public string word { get; set; }
            public string pronunciation { get; set; }
        }
    
}
