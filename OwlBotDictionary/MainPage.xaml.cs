﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace OwlBotDictionary
{
	// Learn more about making custom code visible in the Xamarin.Forms previewer
	// by visiting https://aka.ms/xamarinforms-previewer
	[DesignTimeVisible(false)]
	public partial class MainPage : ContentPage
	{//This page working as an splash screen
		public MainPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}
		protected async override void OnAppearing()
		{//When page is called everytime this method is invoke
			base.OnAppearing();
			await myimage.ScaleTo(1, 2000);
			await myimage.ScaleTo(0.7, 1500, Easing.Linear);
			await myimage.ScaleTo(60, 100, Easing.Linear);
			Application.Current.MainPage = new NavigationPage(new DictionaryPage());
		}
	}
}
