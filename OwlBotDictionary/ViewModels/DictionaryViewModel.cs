﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using OwlBotDictionary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace OwlBotDictionary.ViewModels
{    //This class is bind to DictionaryPage
    public class DictionaryViewModel : INotifyPropertyChanged
    {
        public DictionaryViewModel()
        {

        }

        public Command SearchBarPressed
        {
            get
            {
                return new Command(async () =>
                {
                    //This is Using xamarin.essentials nuget to check internet connectivity
                    if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                    {
                        try
                        {
                            string url = "https://owlbot.info/api/v4/dictionary/" + SearchbarText;

                            HttpClient Client = new HttpClient();
                            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", "c65e660a7827589de5010c07f896b29f72ffe2a6");
                            var response = await Client.GetAsync(url);
                            var json = await response.Content.ReadAsStringAsync();
                            DictionaryModel result = JsonConvert.DeserializeObject<DictionaryModel>(json);
                           
                            SearchWord = Convert.ToString(result.word);
                            SearchWordPronunciation = Convert.ToString(result.pronunciation);
                            var data = result.definitions.Select(x => x.definition).ToList();
                            var data1 = result.definitions.Select(x => x.example).ToList();
                            SearchWordExample = Convert.ToString(data1[0]);
                            SearchWordDefinition = Convert.ToString(data[0]);
                        }
                        catch (Exception ex)
                        {//When We pass wrong word and get null result
                            SearchWord = "Word Not Found";
                            SearchWordPronunciation = null;
                            SearchWordExample = null;
                            SearchWordDefinition = "please check the spelling and check internet Connection";
                            return;
                        }

                    }
                    else
                    {//When connectivity not available
                        UserDialogs.Instance.Toast("Oops, looks like you don't have internet connection :(");
                        return;
                    }
                });
            }
        }
        string _searchword;
        public string SearchWord { get { return _searchword; } set { _searchword = value; OnPropertyChanged(); } }
        string _searchWordPronunciation;
        public string SearchWordPronunciation { get { return _searchWordPronunciation; } set { _searchWordPronunciation = value; OnPropertyChanged(); } }
        string _searchWordDefinition;
        public string SearchWordDefinition { get { return _searchWordDefinition; } set { _searchWordDefinition = value; OnPropertyChanged(); } }
        string _searchWordExample;
        public string SearchWordExample { get { return _searchWordExample; } set { _searchWordExample = value; OnPropertyChanged(); } }

        string _searchbartext;
        public string SearchbarText { get { return _searchbartext; } set { _searchbartext = value; OnPropertyChanged(); } }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(
      [CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this,
            new PropertyChangedEventArgs(propertyName));
        }
    }
}
